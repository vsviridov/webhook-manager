import "dotenv/config";

export class Config {
  static port = getInt("PORT", 3000);
  static host = getString("HOST", "localhost");
  static database = getString("DATABASE", "./data/database.sqlite");
  static prefix = getString("PREFIX", "/webhooks");
}

function getInt(name: string, default_value?: number) {
  if (name in process.env) {
    const { [name]: value } = process.env;
    if (value) {
      return parseInt(value, 10);
    }
  }

  if (default_value !== undefined) {
    return default_value;
  }

  throw new Error(`Neither environment variable nor default value provided for "${name}" setting.`);
}

function getString(name: string, default_value: string) {
  if (name in process.env) {
    const { [name]: value } = process.env;
    if (value !== undefined) {
      return value;
    }
  }

  if (default_value !== undefined) {
    return default_value;
  }

  throw new Error(`Neither environment variable nor default value provided for "${name}" setting.`);
}
