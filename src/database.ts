import DB from "better-sqlite3";
import type { Database as TDatabase } from "better-sqlite3";
import { resolve, basename } from "path";
import { set } from "lodash";
import { readdirSync, readFileSync } from "fs";

import { Brand } from "./brand";

type SchemaType = "table" | "index";
type SchemaState = Record<SchemaType, Record<string, boolean>>;
type DDLItem = { type: SchemaType; name: string; ddl: string };

export type HookId = Brand<string, "hook_id">;
export type Hook = {
  id: number;
  hook_id: HookId;
  hook_secret: string;
  name: string;
  description?: string;
  filter?: string;
  command: string;
  uid: number;
  gid: number;
  cwd: string;
};

function readSchemaDir(type: SchemaType) {
  const prefix = resolve(__dirname, "..", "data", "schema", type);
  const indexes = readdirSync(prefix);

  return indexes.map((file) => ({
    type,
    name: basename(file, ".sql"),
    ddl: readFileSync(resolve(prefix, file), { encoding: "utf8" }),
  }));
}

function executeDDL(schema: SchemaState, db: TDatabase) {
  return function executeDDLInner({ type, name, ddl }: DDLItem) {
    if (schema[type][name]) {
      return;
    }

    console.log(`Executng DDL for ${type}/${name}`);
    db.exec(ddl);
  };
}

export interface MiddlewareOptions {
  path: string;
}

export class Database {
  connection: TDatabase;

  static middleware(options: MiddlewareOptions) {
    return (ctx: { state: Record<string, unknown> }, next: (...args: any[]) => void) => {
      ctx.state.db = new Database(options.path);
      return next();
    };
  }

  constructor(_path: string) {
    const path = resolve(process.cwd(), _path);
    // console.log(`Opening database: ${path}`);
    this.connection = new DB(path, {});

    this.configure();
  }

  getHooks() {
    const statement = this.connection.prepare("SELECT * FROM hooks");

    return statement.all() as Hook[];
  }

  getHookById(id: HookId) {
    const statement = this.connection.prepare("SELECT * FROM hooks WHERE hook_id = ?");

    return statement.get(id) as Hook;
  }

  createHook(hook: Partial<Hook>) {
    const statement = this.connection.prepare(
      `INSERT INTO hooks (hook_id, hook_secret, name, description, command, uid, gid, cwd) VALUES ($hook_id, $hook_secret, $name, $description, $command, $uid, $gid, $cwd) RETURNING *`
    );

    return statement.run(hook);
  }

  private configure() {
    const statement = this.connection.prepare("SELECT name, type FROM sqlite_master");

    const schema = statement
      .all()
      .reduce<SchemaState>((acc, { name, type }) => set(acc, [type, name], true), { index: {}, table: {} });

    readSchemaDir("table").forEach(executeDDL(schema, this.connection));
    readSchemaDir("index").forEach(executeDDL(schema, this.connection));
  }
}
