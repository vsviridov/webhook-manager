import Koa from "koa";
import Router from "@koa/router";
import bodyParser from "koa-bodyparser";
import render from "koa-ejs";
import logger from "koa-logger";
import { spawn } from "child_process";
import { access } from "fs/promises";
import { constants } from "fs";
import { resolve } from "path";
import { v4 } from "uuid";

import { Config } from "./config";
import { getSystemUsers, getSystemGroups } from "./passwd";
import { Database } from "./database";
import type { HookId, Hook } from "./database";
import { authMiddleware, gitlabFilterMiddleware, enhanceViewsMiddleware } from "./middlewares";

interface State {
  db: Database;
}

interface HookState extends State {
  hook: Hook;
}

const server = new Koa<State>({ proxy: true });

const router = new Router<State>({ prefix: Config.prefix });

server.use(logger());
server.use(bodyParser());
server.use(Database.middleware({ path: Config.database }));
router.use(authMiddleware({ except: ["executeHook"] }));
render(server, {
  root: resolve(__dirname, "../views"),
  layout: "template",
  cache: false,
  debug: false,
});
server.use(enhanceViewsMiddleware());

router.param("hookId", (hookId, ctx, next) => {
  const hook = ctx.state.db.getHookById(hookId as HookId);
  if (!hook) {
    ctx.body = { error: "Unknown Hook" };
    return;
  }
  (ctx.state as HookState).hook = hook;
  return next();
});

router.get("/", async (ctx) => {
  await ctx.render("hooks", { hooks: ctx.state.db.getHooks() });
});

router.get("/new", async (ctx) => {
  const [users, groups] = await Promise.all([getSystemUsers(), getSystemGroups()]);
  await ctx.render("new_hook", { users, groups });
});

router.get<HookState>("getHook", "/:hookId", async (ctx) => {
  await ctx.render("hook");
});

router.post("/new", async (ctx) => {
  const newHook: Omit<Hook, "hook_id" | "hook_secret"> = ctx.request.body;

  const hook_id = v4() as HookId;
  const hook_secret = v4();

  const hook = { ...newHook, hook_id, hook_secret };
  ctx.state.db.createHook(hook);

  const redirectPath = router.url("getHook", { hookId: hook_id });

  if (!(redirectPath instanceof Error)) {
    return ctx.redirect(redirectPath);
  }

  throw redirectPath;
});

router.post<HookState>("executeHook", "/:hookId", gitlabFilterMiddleware(), async (ctx) => {
  function returnEarly(response: object) {
    ctx.body = { ...ctx.body, ...response };
  }
  const { name, description, command, uid, gid, cwd } = ctx.state.hook;
  const message = [name, description].filter(Boolean).join("\r\n");
  console.log(`Executing hook ${message}`);

  const users = await getSystemUsers();
  const groups = await getSystemGroups();

  const user = users.find((x) => x.uid === uid);
  const group = groups.find((x) => x.gid === gid);

  if (!user) {
    return returnEarly({ error: `Unknown UID: ${uid}` });
  }

  if (!group) {
    return returnEarly({ error: `Unknown GID: ${gid}` });
  }

  console.log(`Launching command: ${command} as ${user.username}:${group.name}`);

  try {
    await access(cwd, constants.R_OK | constants.W_OK);
  } catch {
    return returnEarly({ error: `CWD is not accessible` });
  }

  try {
    await access(resolve(cwd, command), constants.X_OK);
  } catch {
    return returnEarly({ error: "Command is not runnable" });
  }

  ctx.response.body = await new Promise((_resolve) => {
    const childProcess = spawn(resolve(cwd, command), [], { uid, gid, cwd, env: {} });

    childProcess.stdin.write(JSON.stringify(ctx.request.body), (error) => {
      if (error) {
        console.error(`Failed to write to the process. ${error.message}`);
      }
    });

    childProcess.stdin.end();

    const chunks: string[] = [];
    let stdout: string;

    childProcess.stdout.on("data", (chunk) => {
      chunks.push(chunk.toString());
    });

    childProcess.stdout.on("end", () => {
      stdout = chunks.join("");
    });

    childProcess.once("error", (err) => {
      console.error(`Here: ${err.message}`);
      // reject(err);
      _resolve({ error: err });
    });

    childProcess.once("exit", (code, signal) => {
      console.log(`Child exited: ${code}`);
      _resolve({ code, signal, stdout });
    });
  });
});

server.use(router.routes()).use(router.allowedMethods());
server.listen(Config.port, Config.host, () => {
  console.log(`Application is listening on ${Config.port}`);
});
