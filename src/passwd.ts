import { readFile } from "fs/promises";

interface User {
  username: string;
  uid: number;
  gid: number;
  home: string;
  shell: string;
  isSystem: boolean;
}

interface Group {
  name: string;
  gid: number;
  users: string[];
  isSystem: boolean;
}

export async function getSystemUsers() {
  const fileContents = await readFile("/etc/passwd", { encoding: "utf8" });

  return fileContents
    .split(/[\r\n]/)
    .filter((x) => !!x)
    .reduce<User[]>((acc, i) => {
      return [...acc, parsePasswdLine(i)];
    }, [])
    .sort((a, b) => a.uid - b.uid);
}

export async function getSystemGroups() {
  const fileContents = await readFile("/etc/group", { encoding: "utf8" });

  return fileContents
    .split(/[\r\n]/)
    .filter((x) => !!x)
    .reduce<Group[]>((acc, i) => [...acc, parseGroupLine(i)], [])
    .sort((a, b) => a.gid - b.gid);
}

function parsePasswdLine(line: string): User {
  const [username, , _uid, _gid, , home, shell] = line.split(":");
  const uid = parseInt(_uid, 10);
  const gid = parseInt(_gid, 10);

  return {
    username,
    uid,
    gid,
    home,
    shell,
    isSystem: uid < 1000,
  };
}

function parseGroupLine(line: string): Group {
  const [name, , _gid, _users] = line.split(":");

  const gid = parseInt(_gid, 10);
  const users = _users
    .split(",")
    .map((x) => x.trim())
    .filter((x) => !!x);

  return { name, gid, users, isSystem: gid < 1000 };
}
