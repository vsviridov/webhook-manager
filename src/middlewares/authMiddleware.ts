import { Middleware } from "koa";
import { isPrivate } from "ip";

export function authMiddleware(options: { except?: string[] } = {}): Middleware {
  return function authMiddlewareImpl(ctx, next) {
    if (options.except) {
      if (options.except.includes(ctx._matchedRouteName)) {
        return next();
      }
    }
    if (isPrivate(ctx.ip)) {
      return next();
    }
  };
}
