import { Middleware } from "koa";
import { Config } from "../config";

export function enhanceViewsMiddleware(): Middleware {
  return function enhanceViewsMiddlewareImpl(ctx, next) {
    ctx.state = Object.assign(
      ctx.state,
      {},
      {
        prefix: Config.prefix,
        pathWithPrefix(path: string) {
          return Config.prefix ? `${Config.prefix}${path}` : path;
        },
      }
    );
    return next();
  };
}
