import { DefaultState, Middleware } from "koa";
import { Hook } from "../database";

type HookState = DefaultState & { hook: Hook };

export function gitlabFilterMiddleware(): Middleware<HookState> {
  return async function (ctx, next) {
    if (ctx.state.hook && ctx.state.hook.filter === "gitlab") {
      if (ctx.request.headers["x-gitlab-token"] !== ctx.state.hook.hook_secret) {
        console.log("Invalid hook secret");
        ctx.body = null;
        return;
      }
    }
    return next();
  };
}
